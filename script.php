<?php
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 26/03/18
 * Time: 10:08
 */

header("content-type: application/xml; charset=utf-8");
libxml_use_internal_errors();

/**
 * Get HTML
 * @param $link, website adresss
 * @return bool|string
 */
function getSourceCode($link) {
    $opts = [
        'http' => [
            'header' => "User-Agent:MyAgent/1.0\r\n"
            ]
        ];

    $context = stream_context_create($opts);
    return file_get_contents($link, false, $context);
}

/**
 * Create the DOM document
 * @param string $html, HTML file used
 * @return DOMDocument
 */
function createDOM($html) {
    libxml_use_internal_errors(true);

    $doc = new DOMDocument();
    $doc->loadHTML($html);

    return $doc;
}

/**
 * Get comments
 * @param DOMDocument $docBlog, DOM Document used
 * @return array $results, array with comments content
 */
function getComments($docBlog) {

    // Get all articles
    $xpathBlog = new \DOMXpath($docBlog);
    $articles = $xpathBlog->query('//article/header/h2/a/@href');

    $results = [];

    // For each articles
    foreach($articles as $article) {

        $htmlArticle = getSourceCode($article->value);
        $domArticle = createDOM($htmlArticle);

        // Get all comments
        $xpathArticle = new \DOMXPath($domArticle);
        $comments = $xpathArticle->query("//div[@class='ob-comment']");

        // For each comments
        foreach ($comments as $comment) {

            // Get data needed to create rss feed
            $date = $xpathArticle->query(".//span[@class='ob-date']/text()", $comment);
            $author = $xpathArticle->query(".//span[@class='ob-website']/text()", $comment);
            $content = $xpathArticle->query(".//span[@class='ob-text']/text()", $comment);
            $title = $xpathArticle->query("//div[@class='main single']//h1[@class='post-title']//a/text()");

            // Convert date to RSS date format
            $timestamp = strtotime($date->item(0)->nodeValue);
            $date = date("D, d M Y H:i:s T", $timestamp);

            // Fill the result array
            $results[] = [
                'author' => trim($author->item(0)->nodeValue),
                'date' => $date,
                'content' => trim($content->item(0)->nodeValue),
                'title' => trim($title->item(0)->nodeValue),
                'url' => trim($article->nodeValue) . "#" . $timestamp
            ];
        }
    }

    return $results;
}

/**
 * Create an item on the RSS feed
 * @param $comment, comment to add
 * @return string item created
 */
function createRssItem($comment) {
    $item = "<item>";
    $item .= "<title>" . $comment['title'] . "</title>";
    $item .= "<author>" . $comment['author'] . "</author>";
    $item .= "<link>" . $comment['url'] . "</link>";
    $item .= "<pubDate>" . $comment['date'] . "</pubDate>";
    $item .= "<description>" . $comment['content'] . "</description>";
    $item .= "</item>";

    return $item;
}

/**
 * Create the rss feed
 */
function createRssFeed() {

    $htmlBlog = getSourceCode("http://le-multi-gagnant.over-blog.com/");
    $docBlog = createDOM($htmlBlog);
    $comments = getComments($docBlog);


    $rss = "<rss version=\"2.0\">";
    $rss .= "<channel>";

    foreach ($comments as $comment) {
        $rss .= createRssItem($comment);
    }

    $rss.='</channel>';
    $rss.='</rss>';

    echo $rss;
}


createRssFeed();

